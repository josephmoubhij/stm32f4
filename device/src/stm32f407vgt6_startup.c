// #include "stm32f407xx.h"
#include <stdint.h>

extern uint32_t _stext;
extern uint32_t _etext;
extern uint32_t _sdata;
extern uint32_t _edata;
extern uint32_t _sbss;
extern uint32_t _ebss;
extern uint32_t _estack;

void Reset_Handler(void);

int main(void);

typedef struct IntVectors_s
{
	// Starting stack pointer
	void *pStack;

	// Core
	void (*fnReset)(void);
	void (*fnNMI)(void);
	void (*fnHardFault)(void);
	void (*fnMemManage)(void);
	void (*fnBusFault)(void);
	void (*fnUsageFault)(void);
	void *pReserved1[4];
	void (*fnSVCall)(void);
	void (*fnDebugMonitor)(void);
	void *pReserved2;
	void (*fnPendSV)(void);
	void (*fnSysTick)(void);

	// Peripherals
	void (*fnWWDG)(void);			// 0
	void (*fnPVD)(void);			// 1
	void (*fnTAMP_STAMP)(void);		// 2
	void (*fnRTC_WKUP)(void);		// 3
	void (*fnFLASH)(void);			// 4
	void (*fnRCC)(void);			// 5
	void (*fnEXTI0)(void);			// 6
	void (*fnEXTI1)(void);			// 7
	void (*fnEXTI2)(void);			// 8
	void (*fnEXTI3)(void);			// 9
	void (*fnEXTI4)(void);			// 10
	void (*fnDMA1_Stream0)(void);	// 11
	void (*fnDMA1_Stream1)(void);	// 12
	void (*fnDMA1_Stream2)(void);	// 13
	void (*fnDMA1_Stream3)(void);	// 14
	void (*fnDMA1_Stream4)(void);	// 15
	void (*fnDMA1_Stream5)(void);	// 16
	void (*fnDMA1_Stream6)(void);	// 17
	void (*fnADC)(void);			// 18
	void (*fnCAN1_TX)(void);		// 19
	void (*fnCAN1_RX0)(void);		// 20
	void (*fnCAN1_RX1)(void);		// 21
	void (*fnCAN1_SCE)(void);		// 22
	void (*fnEXTI9_5)(void);		// 23
	void (*fnTIM1_BRK_TIM9)(void);	// 24
	void (*fnTIM1_UP_TIM10)(void);	// 25
	void (*fnTIM1_TRG_TIM11)(void); // 26
	void (*fnTIM1_CC)(void);		// 27
	void (*fnTIM2)(void);			// 28
	void (*fnTIM3)(void);			// 29
	void (*fnTIM4)(void);			// 30
	void (*fnI2C1_EV)(void);		// 31
	void (*fnI2C1_ER)(void);		// 32
	void (*fnI2C2_EV)(void);		// 33
	void (*fnI2C2_ER)(void);		// 34
	void (*fnSPI1)(void);			// 35
	void (*fnSPI2)(void);			// 36
	void (*fnUSART1)(void);			// 37
	void (*fnUSART2)(void);			// 38
	void (*fnUSART3)(void);			// 39
	void (*fnEXTI15_10)(void);		// 40
	void (*fnRTC_Alarm)(void);		// 41
	void (*fnOTG_FS_WKUP)(void);	// 42
	void (*fnTIM8_BRK_TIM12)(void); // 43
	void (*fnTIM8_UP_TIM13)(void);	// 44
	void (*fnTIM8_TRG_TIM14)(void); // 45
	void (*fnTIM8_CC)(void);		// 46
	void (*fnDMA1_Stream7)(void);	// 47
	void (*fnFSMC)(void);			// 48
	void (*fnSDIO)(void);			// 49
	void (*fnTIM5)(void);			// 50
	void (*fnSPI3)(void);			// 51
	void (*fnUART4)(void);			// 52
	void (*fnUART5)(void);			// 53
	void (*fnTIM6_DAC)(void);		// 54
	void (*fnTIM7)(void);			// 55
	void (*fnDMA2_Stream0)(void);	// 56
	void (*fnDMA2_Stream1)(void);	// 57
	void (*fnDMA2_Stream2)(void);	// 58
	void (*fnDMA2_Stream3)(void);	// 59
	void (*fnDMA2_Stream4)(void);	// 60
	void (*fnETH)(void);			// 61
	void (*fnETH_WKUP)(void);		// 62
	void (*fnCAN2_TX)(void);		// 63
	void (*fnCAN2_RX0)(void);		// 64
	void (*fnCAN2_RX1)(void);		// 65
	void (*fnCAN2_SCE)(void);		// 66
	void (*fnOTG_FS)(void);			// 67
	void (*fnDMA2_Stream5)(void);	// 68
	void (*fnDMA2_Stream6)(void);	// 69
	void (*fnDMA2_Stream7)(void);	// 70
	void (*fnUSART6)(void);			// 71
	void (*fnI2C3_EV)(void);		// 72
	void (*fnI2C3_ER)(void);		// 73
	void (*fnOTG_HS_EP1_OUT)(void); // 74
	void (*fnOTG_HS_EP1_IN)(void);	// 75
	void (*fnOTG_HS_WKUP)(void);	// 76
	void (*fnOTG_HS)(void);			// 77
	void (*fnDCMI)(void);			// 78
	void (*fnCRYP)(void);			// 79
	void (*fnHASH_RNG)(void);		// 80
	void (*fnFPU)(void);			// 81
} IntVectors;

void Default_Handler(void);

void NMI_Handler				(void) __attribute__ ((weak, alias("Default_Handler")));
void HardFault_Handler			(void) __attribute__ ((weak, alias("Default_Handler")));
void MemManage_Handler			(void) __attribute__ ((weak, alias("Default_Handler")));
void BusFault_Handler			(void) __attribute__ ((weak, alias("Default_Handler")));
void UsageFault_Handler			(void) __attribute__ ((weak, alias("Default_Handler")));
void SVCall_Handler				(void) __attribute__ ((weak, alias("Default_Handler")));
void DebugMonitor_Handler		(void) __attribute__ ((weak, alias("Default_Handler")));
void PendSV_Handler				(void) __attribute__ ((weak, alias("Default_Handler")));
void SysTick_Handler			(void) __attribute__ ((weak, alias("Default_Handler")));

void WWDG_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 0
void PVD_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 1
void TAMP_STAMP_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 2
void RTC_WKUP_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 3
void FLASH_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 4
void RCC_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 5
void EXTI0_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 6
void EXTI1_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 7
void EXTI2_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 8
void EXTI3_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 9
void EXTI4_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 10
void DMA1_Stream0_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 11
void DMA1_Stream1_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 12
void DMA1_Stream2_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 13
void DMA1_Stream3_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 14
void DMA1_Stream4_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 15
void DMA1_Stream5_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 16
void DMA1_Stream6_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 17
void ADC_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 18
void CAN1_TX_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 19
void CAN1_RX0_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 20
void CAN1_RX1_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 21
void CAN1_SCE_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 22
void EXTI9_5_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 23
void TIM1_BRK_TIM9_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 24
void TIM1_UP_TIM10_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 25
void TIM1_TRG_TIM11_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 26
void TIM1_CC_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 27
void TIM2_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 28
void TIM3_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 29
void TIM4_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 30
void I2C1_EV_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 31
void I2C1_ER_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 32
void I2C2_EV_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 33
void I2C2_ER_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 34
void SPI1_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 35
void SPI2_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 36
void USART1_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 37
void USART2_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 38
void USART3_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 39
void EXTI15_10_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 40
void RTC_Alarm_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 41
void OTG_FS_WKUP_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 42
void TIM8_BRK_TIM12_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 43
void TIM8_UP_TIM13_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 44
void TIM8_TRG_TIM14_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 45
void TIM8_CC_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 46
void DMA1_Stream7_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 47
void FSMC_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 48
void SDIO_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 49
void TIM5_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 50
void SPI3_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 51
void UART4_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 52
void UART5_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 53
void TIM6_DAC_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 54
void TIM7_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 55
void DMA2_Stream0_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 56
void DMA2_Stream1_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 57
void DMA2_Stream2_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 58
void DMA2_Stream3_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 59
void DMA2_Stream4_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 60
void ETH_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 61
void ETH_WKUP_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 62
void CAN2_TX_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 63
void CAN2_RX0_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 64
void CAN2_RX1_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 65
void CAN2_SCE_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 66
void OTG_FS_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 67
void DMA2_Stream5_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 68
void DMA2_Stream6_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 69
void DMA2_Stream7_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 70
void USART6_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 71
void I2C3_EV_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 72
void I2C3_ER_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 73
void OTG_HS_EP1_OUT_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 74
void OTG_HS_EP1_IN_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 75
void OTG_HS_WKUP_Handler		(void) __attribute__ ((weak, alias("Default_Handler"))); // 76
void OTG_HS_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 77
void DCMI_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 78
void CRYP_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 79
void HASH_RNG_Handler			(void) __attribute__ ((weak, alias("Default_Handler"))); // 80
void FPU_Handler				(void) __attribute__ ((weak, alias("Default_Handler"))); // 81

__attribute__ ((section(".vector_table")))
const IntVectors vector_table = {
	.pStack = (void*) (&_estack),

	.fnReset = Reset_Handler,
	.fnNMI = NMI_Handler,
	.fnHardFault = HardFault_Handler,
	.fnMemManage = MemManage_Handler,
	.fnBusFault = BusFault_Handler,
	.fnUsageFault = UsageFault_Handler,
	.pReserved1 = {0UL},
	.fnSVCall = SVCall_Handler,
	.fnDebugMonitor = DebugMonitor_Handler,
	.pReserved2 = 0UL,
	.fnPendSV = PendSV_Handler,
	.fnSysTick = SysTick_Handler,

	.fnWWDG = WWDG_Handler,
	.fnPVD = PVD_Handler,
	.fnTAMP_STAMP = TAMP_STAMP_Handler,
	.fnRTC_WKUP = RTC_WKUP_Handler,
	.fnFLASH = FLASH_Handler,
	.fnRCC = RCC_Handler,
	.fnEXTI0 = EXTI0_Handler,
	.fnEXTI1 = EXTI1_Handler,
	.fnEXTI2 = EXTI2_Handler,
	.fnEXTI3 = EXTI3_Handler,
	.fnEXTI4 = EXTI4_Handler,
	.fnDMA1_Stream0 = DMA1_Stream0_Handler,
	.fnDMA1_Stream1 = DMA1_Stream1_Handler,
	.fnDMA1_Stream2 = DMA1_Stream2_Handler,
	.fnDMA1_Stream3 = DMA1_Stream3_Handler,
	.fnDMA1_Stream4 = DMA1_Stream4_Handler,
	.fnDMA1_Stream5 = DMA1_Stream5_Handler,
	.fnDMA1_Stream6 = DMA1_Stream6_Handler,
	.fnADC = ADC_Handler,
	.fnCAN1_TX = CAN1_TX_Handler,
	.fnCAN1_RX0 = CAN1_RX0_Handler,
	.fnCAN1_RX1 = CAN1_RX1_Handler,
	.fnCAN1_SCE = CAN1_SCE_Handler,
	.fnEXTI9_5 = EXTI9_5_Handler,
	.fnTIM1_BRK_TIM9 = TIM1_BRK_TIM9_Handler,
	.fnTIM1_UP_TIM10 = TIM1_UP_TIM10_Handler,
	.fnTIM1_TRG_TIM11 = TIM1_TRG_TIM11_Handler,
	.fnTIM1_CC = TIM1_CC_Handler,
	.fnTIM2 = TIM2_Handler,
	.fnTIM3 = TIM3_Handler,
	.fnTIM4 = TIM4_Handler,
	.fnI2C1_EV = I2C1_EV_Handler,
	.fnI2C1_ER = I2C1_ER_Handler,
	.fnI2C2_EV = I2C2_EV_Handler,
	.fnI2C2_ER = I2C2_ER_Handler,
	.fnSPI1 = SPI1_Handler,
	.fnSPI2 = SPI2_Handler,
	.fnUSART1 = USART1_Handler,
	.fnUSART2 = USART2_Handler,
	.fnUSART3 = USART3_Handler,
	.fnEXTI15_10 = EXTI15_10_Handler,
	.fnRTC_Alarm = RTC_Alarm_Handler,
	.fnOTG_FS_WKUP = OTG_FS_WKUP_Handler,
	.fnTIM8_BRK_TIM12 = TIM8_BRK_TIM12_Handler,
	.fnTIM8_UP_TIM13 = TIM8_UP_TIM13_Handler,
	.fnTIM8_TRG_TIM14 = TIM8_TRG_TIM14_Handler,
	.fnTIM8_CC = TIM8_CC_Handler,
	.fnDMA2_Stream0 = DMA2_Stream0_Handler,
	.fnDMA2_Stream1 = DMA2_Stream1_Handler,
	.fnDMA2_Stream2 = DMA2_Stream2_Handler,
	.fnDMA2_Stream3 = DMA2_Stream3_Handler,
	.fnDMA2_Stream4 = DMA2_Stream4_Handler,
	.fnETH = ETH_Handler,
	.fnETH_WKUP = ETH_WKUP_Handler,
	.fnCAN2_TX = CAN2_TX_Handler,
	.fnCAN2_RX0 = CAN2_RX0_Handler,
	.fnCAN2_RX1 = CAN2_RX1_Handler,
	.fnCAN2_SCE = CAN2_SCE_Handler,
	.fnOTG_FS = OTG_FS_Handler,
	.fnDMA2_Stream5 = DMA2_Stream5_Handler,
	.fnDMA2_Stream6 = DMA2_Stream6_Handler,
	.fnDMA2_Stream7 = DMA2_Stream7_Handler,
	.fnUSART6 = USART6_Handler,
	.fnI2C3_EV = I2C3_EV_Handler,
	.fnI2C3_ER = I2C3_ER_Handler,
	.fnOTG_HS_EP1_OUT = OTG_HS_EP1_OUT_Handler,
	.fnOTG_HS_EP1_IN = OTG_HS_EP1_IN_Handler,
	.fnOTG_HS_WKUP = OTG_HS_WKUP_Handler,
	.fnOTG_HS = OTG_HS_Handler,
	.fnDCMI = DCMI_Handler,
	.fnCRYP = CRYP_Handler,
	.fnHASH_RNG = HASH_RNG_Handler,
	.fnFPU = FPU_Handler,
};

void Reset_Handler(void)
{
	uint32_t *pSrc = &_etext;
	uint32_t *pDes = &_sdata;
	if (pSrc != pDes)
	{
		for (; pDes < &_edata;)
		{
			*pDes++ = *pSrc++;
		}
	}

	for (pDes = &_sbss; pDes < &_ebss;)
	{
		*pDes++ = 0;
	}

	main();

	while (1)
	{
		continue;
	}
}

void Default_Handler(void)
{
	while (1)
	{
		continue;
	}
}
